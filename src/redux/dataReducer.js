const initState = {
  selections: [],
};

export const UPDATE_SELECTION = 'UPDATE_SELECTION';

export function updateSelectionAction(selections = []) {
  return { type: UPDATE_SELECTION, selections };
}

export default function(state = initState, action) {
  const reduce = {
    [UPDATE_SELECTION]: () => {
      //  replace selection that matches src
      const filteredSelections = state.selections.filter(sel => !action.selections.find(aSel => aSel.src === sel.src));
      return {
        ...state, 
        selections: [...filteredSelections, ...action.selections],
      };
    },
  };
  return reduce[action.type] ? reduce[action.type](action) : state;
}