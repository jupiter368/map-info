const initState = {
  input: '',
  properties: {},
  groups: [],
};

export const UPDATE_FILTER = 'UPDATE_FILTER';

export function updateFilterAction(input, properties, groups) {
  return { type: UPDATE_FILTER, input, properties, groups };
}

export default function(state = initState, action) {
  const reduce = {
    [UPDATE_FILTER]: () => ({
      ...state,
      input: action.input,
      properties: action.properties, 
      groups: action.groups,  
    }),
  };
  return reduce[action.type] ? reduce[action.type](action) : state;
}