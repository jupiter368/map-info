import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import filterReducer from './filterReducer';
import dataReducer from './dataReducer';

const rootReducer = combineReducers({
  filter: filterReducer,
  data: dataReducer,
});
const initState = {};



export default function(firstState = initState) {
  const store = createStore(
    rootReducer,
    firstState,
    compose(
      applyMiddleware(thunk),
      typeof window.__REDUX_DEVTOOLS_EXTENSION__ === 'function'
        ? window.__REDUX_DEVTOOLS_EXTENSION__()
        : f => f,
    )
  );

  return store;
}