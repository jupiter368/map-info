export const filterRegex = /^([a-zA-Z_0-9]+)(?:.(\d+))?(.in|.contains|.gt|.gte|.lt|.lte)?$/;

export function parseRegexGroup(queryParams, regex) {
  const groups = {};
  Object.keys(queryParams).forEach(param => {
    const matched = param.match(regex);
    if (matched && queryParams[param]) {
      const prop = matched[1];
      const group = matched[2] || '0';
      const op = matched[3]; 
      const value = queryParams[param];
      if (!groups[group]) {
        groups[group] = [];
      }
      groups[group].push({ prop, op, value });
    }
  });
  return groups;
}

export function stringToProperties(input) {
  return input.split(',').reduce((acc, segment) => {
    const innerSeg = segment.split('=');
    innerSeg.push(''); // fill in shim value so no need to check length
    innerSeg.push('');
    acc[innerSeg[0].trim()] = innerSeg[1].trim();
    return acc;
  }, {});
}

function applyOperations(op) {
  return {
    '.contains': (a, b) => a.includes(b),
    '.gt': (a, b) => a > b,
    '.gte': (a, b) => a >= b,
    '.lt': (a, b) => a < b,
    '.lte': (a, b) => a <= b,
  }[op] || ((a, b) => a == b); // use == on purpose so string and int compare don't need casting
}

function validateGroup(values, AND) {
  let result = true;
  AND.forEach(filter => {
    // skip when filter doesn't match any value props
    if (values.hasOwnProperty(filter.prop)) {
      // only need one false value to null entire group
      if (!applyOperations(filter.op)(values[filter.prop], filter.value)) {
        result = false;
      }
    }
  });
  return result;
}

export function filterGroups(values, groups) {
  const validated = Object.keys(groups).map(OR => {
    const valid = validateGroup(values, groups[OR]);
    return valid;
  });
  // console.log(values, validated);
  return validated.reduce((final, next) => final || next, !validated.length > 0);
}