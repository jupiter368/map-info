  import React from 'react';
import styled from 'styled-components';
import Filter from './Filter';
import Map from './Map';
import Pie from './Pie';

const FlowDiv = styled.div`
  border: orange 2px solid;
  display: flex;
  height: 100%;
  flex-direction: column;
  margin: 8px;

  .content-div {
    margin: 8px;
  }
`;

const RowDiv = styled.div`
  display: flex;
  height: 100%;

`;

function App(props) {
  return (
    <FlowDiv className="App">
      <Filter className="content-div"/>
      <RowDiv>
        <Map className="content-div" srcId="North DC Map" publish="north"/>
        <Map className="content-div" srcId="South DC Map" publish="south"/>
      </RowDiv>
      <RowDiv>
        <Pie className="content-div" srcId="Selected Incidents" subscribe={['north', 'south']}/>
      </RowDiv>
    </FlowDiv>
  );
}

export default App;
