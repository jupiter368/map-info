import React from 'react';

class Resizable extends React.Component {
  componentDidMount() {
    this.resizeHandle();
    window.addEventListener("load", this.resizeHandle);
    window.addEventListener("resize", this.resizeHandle);
  }

  componentWillUnmount() {
    window.removeEventListener("load", this.resizeHandle);
    window.removeEventListener("resize", this.resizeHandle);
  }

  // default resize to parentNode width and height
  // override to provide custom resize handle
  resizeHandle = () => {
    const oneUp = this.svgE.current.parentNode;
    this.setState({
      ...this.state,
      size: {
        w: oneUp.clientWidth,
        h: oneUp.clientHeight - 20,
      },
    });
  }
}

export default Resizable;