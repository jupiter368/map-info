import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { updateFilterAction } from '../redux/filterReducer';
import { filterRegex, parseRegexGroup, stringToProperties } from '../utils/filter';

const SearchDiv = styled.div`
  display: flex;
  label {
    padding: 4px;
  }
  input {
    flex-grow: 1;
  }
`;


function mapStateToProps({ filter }) {
  return filter;
}

class Filter extends React.Component {
  constructor(props) {
    super(props);
    this.textE = React.createRef();
  }

  changeHandle = () => {
    const input = this.textE.current.value;
    const properties = stringToProperties(input);
    const groups = parseRegexGroup(properties, filterRegex);
    this.props.dispatch(updateFilterAction(input, properties, groups));
  }

  render() {
    return (<SearchDiv className={this.props.className}>
        <label>Search:</label>
        <input
          type="text"
          ref={this.textE} 
          onChange={this.changeHandle}
          value={this.props.search}
          placeholder="e.g <property>=, <property>.gt=, <property>.gte=, <property>.lt=, <property>.lte=, limit="
        />
    </SearchDiv>);
  }
}
export default connect(mapStateToProps)(Filter);
