import React from 'react';
import { geoMercator, geoPath  } from 'd3-geo';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Resizable from './Resizable';
import { updateSelectionAction } from '../redux/dataReducer';
import { filterGroups } from '../utils/filter';

// TODO: hardcode data import, would like to implement DAO to load data base on props.publish
import northData from '../resources/north.json';
import southData from '../resources/south.json';

// TODO: creating fake data map
const allData = {
  north: northData,
  south: southData,
};

const MapCanvas = styled.div`
  flex: 1 1 auto;
  border: blue 1px solid;

  .shape-vector:hover {
    background-color: yellow;
  }
`;

const severity = [
  { threshold: 15, color: '#dc0000' },
  { threshold: 10, color: '#fd8c00' },
  { threshold: 5, color: '#fdc500' },
  { threshold: 1, color: '#00ac46' },
  { threshold: 0, color: '#dedede' },
];

function mapStateToProps({ filter, data }) {
  return { filter, data };
}
class Map extends Resizable {
  constructor(props) {
    super(props);
    this.svgE = React.createRef();
    this.state = { size: { w: 0, h: 0 } };
  }

  // TODO: handle multiselect
  clickHandle = e => {
    const { srcId, publish } = this.props;
    const idx = parseInt(e.target.getAttribute('idx'));
    this.props.dispatch(updateSelectionAction([{ src: srcId, type: publish, value: idx }]));
  }

  drawMap = () => {
    const { srcId, filter, publish, data: { selections } } = this.props;
    const { w, h } = this.state.size;
    const limit = parseInt(filter.properties.limit);
    // TODO: loading fake data for 'publish' variable. If getting remote data, should be base on some query
    const data = { ...allData[publish]};
    data.features = data.features.filter((feat, i) => filterGroups(feat.properties, filter.groups));
    if (!isNaN(limit)) {
      data.features.splice(limit);
    }
    // TODO: will need to support nested array if multiselection is implemented
    const ownSelections = selections.find(sel => sel.src === srcId);
    const projection = geoMercator().fitSize([w, h], data);
    const pathGenerator = geoPath().projection(projection);
    return data.features.map(df => (<g key={df.id}>
      <path    
        idx={df.id}
        d={pathGenerator(df)}
        fill={ severity.find(s => df.properties.total >= s.threshold).color }
        fillOpacity="0.3"
        //TODO: use == on purpose for now to avoid type casting
        stroke={ ownSelections?.value == df.id ? 'blue' : 'black' }
        strokeDasharray={ ownSelections?.value == df.id ? 0 : 3 }
        strokeWidth={ ownSelections?.value == df.id ? 2 : 1 }
        cursor="pointer"
        className="shape-vector"
        onClick={this.clickHandle}
      />
      {/* TODO: this doesn't seem to work */}
      <text>{df.id}</text>
    </g>));
  }


  render() {
    const { srcId, data: { selections }, className } = this.props;
    return (<MapCanvas className={className}>
      <label>{`${srcId} (${(selections.find(sel => sel.src === srcId))?.value})`}</label>
      <svg width={'100%'} height={'100%'} ref={this.svgE}>
        <g>{this.drawMap()}</g>
      </svg>
    </MapCanvas>);
  }
}

export default connect(mapStateToProps)(Map);
