import React from 'react';
import * as d3 from 'd3';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Resizable from './Resizable';

// TODO: hardcode data import, would like to implement DAO to load data base on props.publish
import allData from '../resources/annotatedData.json';


const PieCanvas = styled.div`
  flex: 1 1 auto;
  border: red 1px solid;
`;

function mapStateToProps({ data }) {
  return { data };
}

class Pie extends Resizable {
  constructor(props) {
    super(props);
    this.svgE = React.createRef();
    this.state = { size: { w: 0, h: 0 } };
  }

  // TODO: can use memorized function to reduce duplicated calls
  getSelectionIds() {
    const { data: { selections }, subscribe } = this.props;
    // const subSelections = selections.filter(sel => subscribe.includes(sel.type));
    const subSelect = [];
    selections.forEach(sel => {
      if (subscribe.includes(sel.type)) {
        subSelect.push(sel.value);
      }
    });
    return subSelect;
  }

  getLabels() {
    const IDs = this.getSelectionIds();
    const names = [];
    allData.features.forEach(df => {
      if(IDs.includes(df.id)) {
        names.push(`${df.id}:${df.properties.name}`);
      }
    });
    return names.join(', ');
  }

  drawPie = (radius) => {
    const IDs = this.getSelectionIds();
    const selectedData = allData.features.filter(df => IDs.includes(df.id));
    console.log(selectedData);
    const sum = {
      '2010': 0,
      '2011': 0,
      '2012': 0,
      '2013': 0,
      '2014': 0,
    };
    selectedData.forEach(sd => {
      Object.keys(sum).forEach(year => {
        sum[year] += sd.properties[year];
      });
    });
    const dataIn = Object.keys(sum).map(year => ({ year, value: sum[year]}));

    const createPie = d3.pie().value(d => d.value).sort(null);
    const createArc = d3.arc().innerRadius(0).outerRadius(radius); // todo
    const colors = d3.scaleOrdinal(d3.schemeCategory10);
    const data = createPie(dataIn);
    console.log(data);
    return data.map((d, i) => (<g key={i}>
      <path d={createArc(d)} fill={colors(i)} />
      <text 
        transform={`translate(${createArc.centroid(d)})`}
        fill='white'
      >
        {`${d.data.year} (${d.value})`}
      </text>
    </g>));
  }

  render() {
    const {w, h} = this.state.size;
    const radius = (w > h ? h : w) / 2;
    return (<PieCanvas className={this.props.className}>
      <label>{`${this.props.srcId}: (${this.getLabels()})`}</label>
      <svg width={'100%'} height={'100%'} ref={this.svgE}>
        <g transform={`translate(${w/2} ${h/2})`}>{this.drawPie(radius)}</g>
      </svg>
    </PieCanvas>);
  }
}

export default connect(mapStateToProps)(Pie);

